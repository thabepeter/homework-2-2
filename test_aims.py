from nose.tools import assert_equal
import aims

def test_positive():
    list = [3, 3]
    abs = aims.std(list)
    exp = 0
    assert_equal(abs, exp)
   
def test_negative():
    list = [-3, -4]
    abs = aims.std(list)
    exp = 0.5
    assert_equal(abs, exp)

def test_float():
    list = [0.2, 0.4]
    abs = aims.std(list)
    exp = 0.1
    assert_equal(abs, exp)
def test_negative_positive():
    list = [-2, 4]
    abs = aims.std(list)
    exp = 3
    assert_equal(abs, exp)

